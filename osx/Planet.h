//
//  Planet.h
//  RenderChimpOSX
//
//  Created by Bengt Ericsson on 9/14/12.
//  Copyright 2012 LTH. All rights reserved.
//
#ifndef PLANET_H_
#define PLANET_H_

#include "RenderChimp.h"
#include <string>
namespace assign1 {
    
using std::string;
    
class Planet {
    string          mName;
    Node            *mParent;
    Geometry        *mGeometry;
    Texture         *mTexture;
    Group           *mPivot;
    vec3f           *mAxisVector;
    vec3f           *mOrbitVector;
    f32             mAxisSpeed;
    f32             mOrbitSpeed;
    f32             mPulsePeriod;
    f32             mElapsedSeconds;
public:
    Planet(string &name, Node *parent);    
    void translate(vec3f *v);
    void scale(vec3f *s);
    void setPulsePeriod(f32 rate);
    
    void setShader(ShaderProgram *shader);
    void setTexture(const char *path);
    
    void setAxisRotation(f32 angle);
    void updateAxisRotation(f32 time);
    
    void setOrbitRotation(f32 angle);
    void updateOrbitRotation(f32 time);
    
    void update(f32 time);
    
};
}

#endif
