//
//  Planet.cpp
//  RenderChimpOSX
//
//  Created by Bengt Ericsson on 9/14/12.
//  Copyright 2012 LTH. All rights reserved.
//

#include "Planet.h"
#include "RenderChimp.h"
#include <iostream>
using std::cout;
using std::endl;
namespace assign1 {
    
    
    Planet::Planet(string &name, Node *parent) : mName(name) 
    {        
        mGeometry = SceneGraph::createGeometry(mName.c_str(), "scenes/sphere.obj");
        mPivot = SceneGraph::createGroup((mName+"_pivot").c_str());
        mPivot->attachChild(mGeometry);
        parent->attachChild(mPivot);
    }
    
    void Planet::setPulsePeriod(f32 rate)
    {
        mPulsePeriod = rate;
    }
    
    void Planet::translate(vec3f *v) 
    {
        mGeometry->translate(*v);
    }
    
    void Planet::scale(vec3f *s)
    {
        mGeometry->scale(*s);
    }
    
    void Planet::setShader(ShaderProgram *shader)
    {
        mGeometry->setShaderProgram(shader);
    }
    
    void Planet::setTexture(const char* path) 
    {
        Texture *texture = SceneGraph::createTexture((mName + "_texture").c_str(), 
                                                     path, true, TEXTURE_FILTER_TRILINEAR, 
                                                     TEXTURE_WRAP_REPEAT);
        mGeometry->setTexture("DiffuseTex", texture);

    }
    
    void Planet::setAxisRotation(f32 angle, vec3f *v)
    {
        mAxisSpeed = angle;
        mAxisVector = v;
    }
    
    void Planet::setOrbitRotation(f32 angle, vec3f *v)
    {
        mOrbitSpeed = angle;
        mOrbitVector = v;
    }
    
    void Planet::update(f32 time) 
    {
        // TODO: ROTATE :D
        updateAxisRotation(time);
        updateOrbitRotation(time);
        
        mElapsedSeconds += time;
        // 2π*2.5 
        //mGeometry->setScale(0.5f + cos(mElapsedSeconds * 3.1459) * 0.25f);
    }
    
    void Planet::updateAxisRotation(f32 time)
    {
        mGeometry->rotate(mAxisSpeed * time, *mAxisVector);
    }
    
    void Planet::updateOrbitRotation(f32 time)
    {
        mPivot->rotate(mOrbitSpeed * time, *mOrbitVector);
        
    }
}