#version 150

uniform samplerCube CubeMap;

in vec3 normal;
in vec3 view;

out vec4 fragColor;

void main() {

	vec3 N = normalize(normal);
	vec3 V = normalize(view);
	vec3 R = reflect(-V, N);

	vec4 reflection = texture(CubeMap, R);

	fragColor = reflection;
}
