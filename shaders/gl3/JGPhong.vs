#version 150
/* Per vertex Phong shader */

/* 
 * Uniform variables set automatically by RC
 */
 
uniform mat4 World;			/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;	/* transform to world space (normals) */

/*
 * Uniform variables to set manually, with shaderprogram->setVector(...)
 */
 
uniform vec3 LightColor;
uniform vec3 LightPosition;
uniform vec3 ViewPosition;

/*
 * Attributes with same name as in Vertex Array, e.g:
 *	torus_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
 *	torus_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
 *	torus_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
 *	torus_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
 *	torus_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);
 * => INPUT of vertex shader
*/

in vec3 Vertex;
in vec3 Normal;


/*
 * Varying variables will be sent to pixel shader. => OUTPUT of vertex shader
 */
 
out vec3 fN;
out vec3 fV;
out vec3 fL;
out vec2 texcoord;



void main()
{	
	vec4 v		= vec4(Vertex,1);
	vec4 n		= vec4(Normal,0);	
	vec3 worldPos	= ( World * v ).xyz;
	
	fN = ( WorldInverseTranspose * n ).xyz;
	fV = ViewPosition - worldPos;
	fL = LightPosition - worldPos;

	gl_Position = WorldViewProjection * v;		
}
