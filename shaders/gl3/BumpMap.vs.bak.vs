#version 150

/* Per vertex Lambert shader */

/* variables per vertex
*/
in vec3 Vertex;
in vec3 Texcoord;
in vec3 Normal;
in vec3 Tangent;
in vec3 Binormal;

/* variables per primitive
*/
uniform mat4 World;					/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;	/* transform to world space (normals) */

uniform vec3 ViewPosition;
uniform vec3 LightPosition;
uniform vec3 LightColor;

/* Scale uniform
*/
uniform vec2 Scale;

/* 'out' variables are interpolated over the triangle
	and available per pixel in the fragment shader
 */
out vec3 DiffuseColor;
out vec3 light;
out vec3 normal;
out vec3 tangent;
out vec3 binormal;
out vec2 texcoord;

void main() {

	vec3 worldPosition = (World * vec4(Vertex.xyz, 1.0)).xyz;
	vec3 worldNormal = (WorldInverseTranspose * vec4(Normal.xyz, 1.0)).xyz;
	vec3 viewVector = normalize(ViewPosition - worldPosition);

	/* diffuse shading
	*/
	vec3 lightVector = normalize(LightPosition - worldPosition);
	float lightNormalCos = max(0.0, dot(normalize(worldNormal), lightVector));


	/* Set output values
	*/
	light = lightVector;
	normal = Normal;
	tangent = Tangent;
	binormal = Binormal;

	DiffuseColor = LightColor * lightNormalCos;

	texcoord = vec2(Texcoord.x, Texcoord.y);

	texcoord = Scale * texcoord;

	/* set the final position for this vertex
	*/
	gl_Position = WorldViewProjection * vec4(Vertex.xyz, 1.0);

}



