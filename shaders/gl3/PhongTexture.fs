#version 150

uniform sampler2D DiffuseTex;

uniform vec3 AmbientColor;
uniform vec3 SpecularColor;
uniform vec3 ViewPosition;
uniform vec3 LightPosition;
uniform float Shininess;

in vec3 DiffuseColor;
in vec3 outNormal;
in vec3 outLightNormalCos;
in vec2 texcoord;

out vec4 fragColor;

void main() {


	vec3 N = normalize(outNormal);
	vec3 L = normalize(outLightNormalCos);
	vec3 V = normalize(ViewPosition);

	float ka = 1.0;
	float kd = 1.0;
	float ks = 1.0;
	float shininess = 20.0;

	vec4 fg = 	vec4(
				(ka * AmbientColor) +
				(kd * DiffuseColor * max(dot(L,N), 0.0)) +
				//(kd * DiffuseColor * max(dot(L, N), 0.0) * ) +
				(ks * SpecularColor * max(pow(dot(reflect(-L, N), V), shininess), 0.0)),
				1.0);


	fg *= texture2D(DiffuseTex, texcoord);

	/* write final color to buffer
	*/
	fragColor = fg;
}
