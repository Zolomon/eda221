#version 150

/*
 * Uniform variables to set manually, with shaderprogram->setVector(...)
 */

uniform mat4 World;			/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;	/* transform to world space (normals) */

uniform samplerCube CubeMap;
uniform sampler2D BumpMap;

/* Varying variables are handed from vertex shader.
 * => INPUT for pixel shader from vertex shader
 */
 
in vec3 fN;		// un-normalized normal 
in vec3 fL;		// un-normalized light
in vec3 fV;		// un-normalized view
in vec3 fB;     // un-normalized binormal
in vec3 fT;     // un-normalized tangent
in vec2 texcoord; // texture coordinate

out vec4 fragColor;

void main()
{	
	vec3 N = normalize(fN);
	vec3 L = normalize(fL);
	vec3 V = normalize(fV);
    vec3 T = normalize(fT);
    vec3 B = normalize(fB);
    mat3 TBN = mat3(T,B,N);

    vec3 bumpColor    = texture(BumpMap, texcoord).xyz;

    bumpColor = bumpColor * 2.0 - 1.0; // Scale to [-1,1]

    bumpColor = TBN * bumpColor; // To Model space
    vec3 bumpNormal = ( WorldInverseTranspose * vec4(bumpColor, 0.0) ).xyz; // To World Space

    bumpNormal = normalize(bumpNormal);

    /*vec3 N = normalize(normal);
	vec3 V = normalize(view);
	vec3 R = reflect(-V, N);

	vec4 reflection = texture(CubeMap, R);*/

    vec3 cubereflect = reflect(-V, T);

    vec4 reflection = texture(CubeMap, cubereflect);

	fragColor = reflection;  //<-- doesn't work
    //fragColor = vec4(bumpNormal, 1.0); // <-- works!
}
