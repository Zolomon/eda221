#version 150

uniform sampler2D DiffuseTex;
uniform sampler2D BumpMap;

uniform vec3 AmbientColor;
uniform vec3 SpecularColor;
uniform vec3 ViewPosition;
uniform float Shininess;

in vec3 DiffuseColor;
in vec3 normal;
in vec3 light;
in vec3 tangent;
in vec3 binormal;
in vec2 texcoord;

out vec4 fragColor;

void main() {


	vec3 N = normalize(normal);
	vec3 L = normalize(light);
	vec3 V = normalize(ViewPosition);

	float ka = 1.0;
	float kd = 1.0;
	float ks = 1.0;

	vec4 color = texture(BumpMap, texcoord);

	vec3 nn = 	   (((color.r*2)-1) * tangent  *  1+
					((color.g*2)-1) * binormal *  1+
					((color.b*2)-1) * normal)  *  1;

	//nn = normalize(nn);

	//vec4 fg = 	vec4(
	//			(ka * AmbientColor) +
	//			(kd * DiffuseColor * max(dot(L,nn), 0.0)) +
	//			(ks * SpecularColor * max(pow(dot(reflect(-L, nn), V), 100), 0.0)),
	//			1.0);
    //
    //
	//fg *= texture(DiffuseTex, texcoord);

	/* write final color to buffer
	*/
	fragColor = fg;
	//fragColor = vec4(nn, 1.0);
}
