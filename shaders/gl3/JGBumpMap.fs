#version 150
/* Phong Pixel Shader */


/*
 * Uniform variables to set manually, with shaderprogram->setVector(...)
 */

uniform mat4 World;			/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;	/* transform to world space (normals) */

uniform vec3 AmbientColor;		// ambient color
uniform vec3 DiffuseColor;		// diffuse color
uniform vec3 SpecularColor;		// specular color
uniform float Shininess;	// shininess ~ sharpness of specular
uniform sampler2D DiffuseTex;
uniform sampler2D BumpMap;

/* Varying variables are handed from vertex shader.
 * => INPUT for pixel shader from vertex shader
 */
 
in vec3 fN;		// un-normalized normal 
in vec3 fL;		// un-normalized light
in vec3 fV;		// un-normalized view
in vec3 fB;
in vec3 fT;
in vec2 texcoord;

out vec4 fragColor;

void main()
{	
	vec3 N = normalize(fN);
	vec3 L = normalize(fL);
	vec3 V = normalize(fV);
    vec3 T = normalize(fT);
    vec3 B = normalize(fB);
    mat3 TBN = mat3(T,B,N);

	vec3 textureColor = texture(DiffuseTex, texcoord).xyz;
    vec3 bumpColor    = texture(BumpMap, texcoord).xyz;

    bumpColor = bumpColor * 2.0 - 1.0; // Scale to [-1,1]

    bumpColor = TBN * bumpColor; // To Model space
    vec3 bumpNormal = ( WorldInverseTranspose * vec4(bumpColor, 0.0) ).xyz; // To World Space

    bumpNormal = normalize(bumpNormal);
    vec3 R = normalize( reflect(-L,bumpNormal) );
	vec3 diffuse	= max( dot(L,bumpNormal), 0.0 ) * textureColor;
	vec3 specular	= SpecularColor * pow( max(dot(V,R), 0.0), Shininess);
	fragColor	= vec4(AmbientColor * textureColor + diffuse + specular * textureColor, 1);
}
