#version 150

uniform sampler2D DiffuseTex;
uniform sampler2D BumpMap;

uniform vec3 AmbientColor;
uniform vec3 SpecularColor;
uniform vec3 ViewPosition;
uniform float Shininess;

in vec3 DiffuseColor;
in vec3 normal;
in vec3 light;
in vec3 tangent;
in vec3 binormal;
in vec2 texcoord;
in vec3 view;
out vec4 fragColor;

void main() {

	const float maxVariance = 2.0; // Mess around with this value to increase/decrease normal perturbation
	const float minVariance = maxVariance / 2.0;

	vec3 N = normalize(normal);
	vec3 L = normalize(light);
	vec3 V = normalize(view);

	float ka = 1.0;
	float kd = 1.0;
	float ks = 1.0;

	vec3 base = texture(DiffuseTex, texcoord).rgb;
	vec3 color = texture(BumpMap, texcoord).xyz;
	vec3 bump = 	((color.r*2)-1) * tangent  *  1+
					((color.g*2)-1) * binormal *  1+
					((color.b*2)-1) * normal   *  1;

	//bump = normalize(bump);

	vec4 fg = 	vec4(
				(ka * AmbientColor * base) +
				(kd * DiffuseColor * max(dot(L, bump), 0.0) * base * 2) +
				(ks * vec3(0,1,0) * max(pow(dot(reflect(-L, bump), V), 10), 0.0) * base),
				1.0);

	/* write final color to buffer
	*/
	fragColor = fg;
}
