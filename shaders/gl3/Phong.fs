#version 150

uniform vec3 AmbientColor;
uniform vec3 SpecularColor;
uniform vec3 ViewPosition;
uniform vec3 LightPosition;

uniform float Shininess;

in vec3 diffuse_color;
in vec3 normal;
in vec3 light;

out vec4 fragColor;

void main() {


	vec3 N = normalize(normal);
	vec3 L = normalize(light);
	vec3 V = normalize(ViewPosition);

	float ka = 1.0;
	float kd = 1.0;
	float ks = 1.0;

	vec4 fg = 	vec4(
				(ka * AmbientColor) +
				(kd * diffuse_color * max(dot(L,N), 0.0)) +
				(ks * SpecularColor * max(pow(dot(reflect(-L, N), V), Shininess), 0.0)),
				1.0);

	/* write final color to buffer
	*/
	fragColor = fg;
}
