#version 150

uniform samplerCube CubeMap;
uniform sampler2D BumpMap;

in vec3 DiffuseColor;
in vec3 normal;
in vec3 light;
in vec3 tangent;
in vec3 binormal;
in vec2 texcoord;
in vec3 view;
out vec4 fragColor;

void main() {

	vec3 N = normalize(normal);
	vec3 L = normalize(light);
	vec3 V = normalize(view);

	vec3 color = texture(BumpMap, texcoord).xyz;
	vec3 bump = 	((color.r*2)-1) * tangent  *  1+
					((color.g*2)-1) * binormal *  1+
					((color.b*2)-1) * normal   *  1;

	vec3 R = reflect(-V, bump);
	//vec3 R = reflect(-V, N);
	vec4 reflection = texture(CubeMap, R.rgb);

	/* write final color to buffer
	*/
	fragColor = reflection;
	//fragColor = vec4(R, 1.0);
}
