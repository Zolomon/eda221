#version 150
/* Phong Pixel Shader */


/*
 * Uniform variables to set manually, with shaderprogram->setVector(...)
 */

uniform vec3 AmbientColor;		// ambient color
uniform vec3 DiffuseColor;		// diffuse color
uniform vec3 SpecularColor;		// specular color
uniform float Shininess;	// shininess ~ sharpness of specular
uniform sampler2D DiffuseTex;

/* Varying variables are handed from vertex shader.
 * => INPUT for pixel shader from vertex shader
 */
 
in vec3 fN;		// un-normalized normal 
in vec3 fL;		// un-normalized light
in vec3 fV;		// un-normalized view
in vec2 texcoord;

out vec4 fragColor;

void main()
{	
	vec3 N = normalize(fN);
	vec3 L = normalize(fL);
	vec3 V = normalize(fV);
	vec3 R = normalize( reflect(-L,N) );

	vec3 textureColor = texture(DiffuseTex, texcoord).xyz;

	vec3 diffuse	= max( dot(L,N), 0.0 ) * textureColor;
	vec3 specular	= SpecularColor * pow( max(dot(V,R), 0.0), Shininess);
	fragColor	    = vec4(AmbientColor * textureColor + diffuse + specular, 1);
}
