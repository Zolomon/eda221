//
//  Planet.h
//  RenderChimpOSX
//
//  Created by Bengt Ericsson on 9/14/12.
//  Copyright 2012 LTH. All rights reserved.
//
#ifndef PLANET_H_
#define PLANET_H_

#include "RenderChimp.h"
#include "ParametricShapes.h"
#include <string>
#include <vector>

namespace assign1 {
    
using std::string;
using std::vector;

class Planet {
    string                   mName;
    Node                    *mParent;
    Geometry                *mGeometry;
    Texture                 *mTexture;
    Geometry                *mRingsTop;
    Geometry                *mRingsBot;
    Texture                 *mRingsTexture;
    Group                   *mPivot;
    vec3f                   *mAxisVector;
    vec3f                   *mOrbitVector;
    f32                      mAxisSpeed;
    f32                      mOrbitSpeed;
    f32                      mPulsePeriod;
    f32                      mElapsedSeconds;
    bool                     mHasRing;
    vector<Group *>          mMoonPivots;
    vector<Geometry *>       mMoons;
    vector<f32>              mMoonAxisVelocities;
    vector<f32>              mMoonOrbitVelocities;
    int                      mMoonCount;
public:
    Planet(string &name, Node *parent);    
    void translate(vec3f *v);
    void translate(const f32 x, const f32 y, const f32 z);
    void scale(vec3f *s);
    void scale(const f32 x, const f32 y, const f32 z);
    void rotate(const f32 angle, vec3f *r);
    void setPulsePeriod(const f32 rate);
    Geometry *getGeometry();

    Node *getPivotNode();

    void setShader(ShaderProgram *shader);
    void setTexture(const char *path);

    void setAxisRotation(const f32 angle);
    void updateAxisRotation(const f32 time);
    void setAxisTilt(const f32 angle);
    
    void setOrbitRotation(const f32 angle);
    void updateOrbitRotation(const f32 time);
    void setOrbitTilt(const f32 angle);

    void updateMoonRotations(const f32 time);
    
    void update(const f32 time);
    
    void attachChild(Node *n);
    void setRings(const char *path, ShaderProgram *shaderprogram, const f32 axisAngle, const i32 res_radius, const i32 res_theta, const f32 inner_radius, const f32 outer_radius);
    void addMoon( const char* path, ShaderProgram * shader, const f32 size, const f32 orbit_radius, const f32 axis_angle, const f32 axis_speed, const f32 orbit_angle, const f32 orbit_speed);
};
}

#endif
