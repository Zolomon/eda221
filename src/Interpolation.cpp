

#include "RenderChimp.h"

/*
	Linear interpolation
		
	Author(s):
*/
vec3f evalLERP(vec3f &p0, vec3f &p1, const f32 x)
{
	/*
	 *	p(x) = [1 x] [1 0; -1 1] [p{i}; p{i+1}]
	 *	     = [(1-x) x] [p{i}; p{i+1}] 
	 *	     = [(1-x)p{i}; xp{i+1}]
	 * ... convex combination of two points
	 */	
	return (1-x)*p0 + x*p1;
}


/*
	Catmull-Rom spline interpolation

	Author(s):
*/
vec3f evalCatmullRom(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x)
{
	/*
	 * [ (-tx + 2tx^2 -tx^3) (1 + (t-3)x^2 + (2-t)x^3) (tx + (3-2t)x^2 + (t-2)x^3) (-tx^2 + tx^3) ] [p0; p1; p2; p3]
	 *
	 */
	f32 a0, a1, a2, a3, x2, x3, x4;
	x2 = x * x;
	x3 = x * x2;
	x4 = x2 * x2;
	
	a0 = -t*x + 2*t*x2 - t*x3;
	a1 = 1 + (t-3)*x2 + (2-t)*x3;
	a2 = t*x + (3-2*t)*x2 + (t-2)*x3;
	a3 = -t*x2 + t*x3;
	return a0*p0 + a1*p1 + a2*p2 + a3*p3;
}
