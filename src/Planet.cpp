//
//  Planet.cpp
//  RenderChimpOSX
//
//  Created by Bengt Ericsson on 9/14/12.
//  Copyright 2012 LTH. All rights reserved.
//

#include "Planet.h"
#include "RenderChimp.h"
#include <iostream>
#include <sstream>

using std::cout;
using std::endl;
using std::stringstream;

namespace assign1 {
    
    
    Planet::Planet(string &name, Node *parent) : mName(name) 
    {        
        mMoonAxisVelocities = vector<f32>();
        mMoonOrbitVelocities = vector<f32>();
        mMoonPivots = vector<Group *>();
        mMoons = vector<Geometry *>();

        mGeometry = SceneGraph::createGeometry(mName.c_str(), "scenes/sphere.obj");
        mPivot = SceneGraph::createGroup((mName+"_pivot").c_str());
        mPivot->attachChild(mGeometry);
        parent->attachChild(mPivot);
    }
    
    Geometry *Planet::getGeometry()
    {
        return mGeometry;
    }

    void Planet::setPulsePeriod(const f32 rate)
    {
        mPulsePeriod = rate;
    }
    
    void Planet::translate(vec3f *v) 
    {
        mGeometry->translate(*v);
    }

    void Planet::translate(const f32 x, const f32 y, const f32 z)
    {
        mGeometry->translate(x,y,z);
    }
    
    void Planet::scale(vec3f *s)
    {
        mGeometry->scale(*s);
    }

    void Planet::scale(const f32 x, const f32 y, const f32 z)
    {
        mGeometry->scale(x,y,z);
    }
    
    void Planet::rotate(const f32 angle, vec3f *r)
    {
        mGeometry->rotate(angle, *r);
    }
    
    void Planet::setShader(ShaderProgram *shader)
    {
        mGeometry->setShaderProgram(shader);
    }
    
    void Planet::setTexture(const char* path) 
    {
        Texture *texture = SceneGraph::createTexture((mName + "_texture").c_str(), 
                                                     path, true, TEXTURE_FILTER_TRILINEAR, 
                                                     TEXTURE_WRAP_REPEAT);
        mGeometry->setTexture("DiffuseTex", texture);
    }
    
    void Planet::setAxisRotation(const f32 angle)
    {
        mAxisSpeed = angle;
    }
    
    void Planet::setOrbitRotation(const f32 angle)
    {
        mOrbitSpeed = angle;
    }

    void Planet::setAxisTilt(const f32 angle)
    {
        mGeometry->rotateX(angle);
    }

    void Planet::setOrbitTilt(const f32 angle)
    {
        mPivot->rotateX(angle);
    }

    void Planet::update(const f32 time) 
    {
        // TODO: ROTATE :D
        updateAxisRotation(time);
        updateOrbitRotation(time);
        updateMoonRotations(time);
        
        mElapsedSeconds += time;
        // 2π*2.5 
        //mGeometry->setScale(0.5f + cos(mElapsedSeconds * 3.1459) * 0.25f);
    }
    
    void Planet::updateAxisRotation(const f32 time)
    {
        mGeometry->rotate(mAxisSpeed*time, mGeometry->getLocalUp());
    }
    
    void Planet::updateOrbitRotation(const f32 time)
    {
        mPivot->rotate(mOrbitSpeed*time, mPivot->getLocalUp());
    }

    void Planet::attachChild(Node *n)
    {
        mGeometry->attachChild(n);
    }

    void Planet::setRings(const char *path, ShaderProgram *shaderprogram, const f32 axisAngle, const i32 res_radius, const i32 res_theta, const f32 inner_radius, const f32 outer_radius) {
        stringstream ss;
                
        string ring_name = mName+"_rings";

        VertexArray *ring_va = createCircleRing(res_radius, res_theta, inner_radius, outer_radius);
        mRingsTop = SceneGraph::createGeometry((ring_name+"_top").c_str(), ring_va);
        mRingsBot = SceneGraph::createGeometry((ring_name+"_bot").c_str(), ring_va);;

        mRingsTexture = SceneGraph::createTexture((ring_name+"_texture").c_str(), path, true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
        mRingsTop->setTexture("DiffuseTex", mRingsTexture );
        mRingsTop->setShaderProgram(shaderprogram);
        mRingsBot->setTexture("DiffuseTex", mRingsTexture );
        mRingsBot->setShaderProgram(shaderprogram);

        mRingsTop->rotateX(-fPI/2);
        mRingsBot->rotateX(fPI/2);
        mRingsTop->rotateZ(axisAngle);
        mRingsTop->rotateZ(axisAngle);
        mGeometry->attachChild(mRingsTop);
        mGeometry->attachChild(mRingsBot);
        mHasRing = true;
    }

    void Planet::updateMoonRotations(const f32 time)
    {
        size_t i = 0;
        for (;i < mMoons.size(); i++)
        {
            mMoons[i]->rotate(mMoonAxisVelocities[i], mMoons[i]->getLocalUp());
            mMoonPivots[i]->rotate(mMoonOrbitVelocities[i], mMoonPivots[i]->getLocalUp());
        }
    }

    void Planet::addMoon( const char* path, ShaderProgram * shader, const f32 size, const f32 orbit_radius, const f32 axis_angle, const f32 axis_speed, const f32 orbit_angle, const f32 orbit_speed)
    {
        mMoonAxisVelocities.push_back( axis_speed );
        mMoonOrbitVelocities.push_back( orbit_speed );

        stringstream ss;
        ss << mName << "_moon_" << mMoonPivots.size() << "_pivot";
        Group *moon_pivot = SceneGraph::createGroup(ss.str().c_str());
        mMoonPivots.push_back(moon_pivot);

        ss.clear();
        ss << mName << "_moon_" << mMoons.size();
        Geometry *moon = SceneGraph::createGeometry(ss.str().c_str(), "scenes/sphere.obj");
        
        ss << "_texture";
        Texture *texture = SceneGraph::createTexture(ss.str().c_str(), 
            path, true, TEXTURE_FILTER_TRILINEAR, 
            TEXTURE_WRAP_REPEAT);
        moon->setShaderProgram(shader);
        moon->setTexture("DiffuseTex", texture);

        mMoons.push_back(moon);

        moon->scale(size);
        moon->translate(orbit_radius,0,0);
        moon->rotateX(axis_angle);

        moon_pivot->attachChild(moon);
        moon_pivot->rotateX(orbit_angle);
        mGeometry->attachChild(moon_pivot);  

    }

    Node *Planet::getPivotNode()
    {
        return mPivot;
    }

}