/*
 *  CyclicMovement.cpp
 *  RenderChimpOSX
 *
 *  Created by Jakob Grundström on 2012-09-22.
 *  Copyright 2012 jakobgrundstrom. All rights reserved.
 *
 */

#include "CyclicMovement.h"


namespace assign2 {

	
CyclicMovement::CyclicMovement(const char * name, Node *parent, vec3f *cp, i32 n) : velocity(10), tension(0.5f), xval(0.0f), mode(LERP) {	
	cycle_group = SceneGraph::createGroup(name);	
	parent->attachChild(cycle_group);
	setControlPoints(cp, n);
	next_point = getIncrement(current_point);
	if (mode == CATMULROM) {
		prev_point	= getDecrement(current_point);
		nextnext_point	= getIncrement(next_point);	
	}
	cycle_objects = vector<Transformable*>();
}

CyclicMovement::~CyclicMovement() {
	delete[] dist;
}

void CyclicMovement::setControlPoints(vec3f *cp, i32 n) {
	npoints		= n;
	control_points  = cp;
	current_point = 0;
	next_point	= getIncrement(current_point);
	
	dist = new f32[n];

	i32 curr = 0, next;
	do {					
		next = getIncrement(curr);		
		dist[curr] = (control_points[next] - control_points[curr]).length();		
		curr = next;
	} while (curr);
	
	cycle_group->setTranslate(control_points[current_point]);
	cycle_group->lookTowards(control_points[next_point]-control_points[current_point], cycle_group->getLocalUp());
}

void CyclicMovement::setInterpolationMode(InterpolationType mode) {
	this->mode = mode;
	if (mode == CATMULROM) {
		prev_point	= getDecrement(current_point);
		nextnext_point	= getIncrement(next_point);	
	}
}

void CyclicMovement::setVelocity(f32 velocity) {
	this->velocity = velocity;
}

void CyclicMovement::setTension(f32 tension) {
	this->tension = tension;
}

void CyclicMovement::attachChild(Transformable *t) {
	cycle_group->attachChild(t);
	cycle_objects.push_back(t);
}

void CyclicMovement::detachChild(Transformable *t) {
	t->detachFromParent();	
	for (int i = 0; i < cycle_objects.size(); ++i) {
		if (cycle_objects[i] == t) {					
			cycle_objects.erase(cycle_objects.begin() + i);
			break;
		}
	}	
}

void CyclicMovement::update(f32 time) 
{	
	vec3f T;	
	f32 dx;
	
	dx = (time*velocity)  / dist[current_point];
	xval += dx;
				
	
	switch (mode) {
		case LERP:
			if (xval >= 1.0f) {
				current_point	= next_point;
				next_point	= getIncrement(next_point);
				xval		= 0;				
			}
//			printf("%d -> %d :: x = %f\n", current_point, next_point, xval);
			T = evalLERP(control_points[current_point], control_points[next_point], xval);
			break;
		case CATMULROM:
			if (xval >= 1.0f) {
				prev_point	= current_point;
				current_point	= next_point;
				next_point	= nextnext_point;
				nextnext_point  = getIncrement(next_point);
				xval		= 0;						
			}
			
//			printf("{%d} -> %d -> %d -> {%d} :: x = %f\n", i0, current_point, next_point, i2, xval);
			T = evalCatmullRom(control_points[prev_point], 
					   control_points[current_point], 
					   control_points[next_point], 
					   control_points[nextnext_point], 
					   tension, 
					   xval);
			break;
		default:
			exit(1);
			break;
	}

	vec3f tangent(0,0,0);

    vec3f pp = control_points[prev_point];
    vec3f np = control_points[next_point];
    tangent = np - pp;

    vec3f nextTangent(0,0,0);

    pp = control_points[current_point];
    np = control_points[nextnext_point];

    nextTangent = np-pp;

    tangent = evalLERP(tangent, nextTangent, xval);
    
	tangent = tangent.normalize();

    cycle_group->setTranslate(T);
    for (int i = 0; i < cycle_objects.size(); ++i) {		

		f32 len = (tangent - vec3f(0,1,0)).len();

		if (len > 0.001f) 
		{
			cycle_objects[i]->lookTowards(-tangent, vec3f(0,1,0));
		}
        
    }
}

}