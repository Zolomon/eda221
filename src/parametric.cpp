
#include "program.h"

#if defined ASSIGNMENT_23

/*
	EDA221 Introduction to Computer Graphics, 2011

	Assignment 2 - Parametric Shapes
*/

#include "RenderChimp.h"
#include "Command.h"
#include "ParametricShapes.h"
#include "CyclicMovement.h"
#include "planet.h"
#include <stdio.h>
#include <utility>
#include <sstream>

using namespace assign1;
using namespace assign2;
using namespace std;


/* variables used for mouse-camera control */
vec2f			camera_rotation(0.0f, 0.0f);	/* current rotation of camera */
vec2f			mouse_prev_pos(0.0f, 0.0f);	/* previous mouse position */

/* Scene graph stuff */
World			*world;				               /* Scene graph root node */
Camera			*camera;			               /* Scene camera */
Group			*camera_pivot;			           /* A group is an empty, transformable node */
Light			*light;				               /* A light is a point in the scene which can be used for lighting calculations */
vec3f			ambient(0.3f, 0.3f, 0.3f);	       /* Ambient color for the sun (never gets dark) */
vec3f           specular(1.0f,0.0f,0.0f);          /* Specular color for the Phong shader*/
vec2f           shape_texture_scale(3.0f, 9.0f);  /* Scale for the texture */
vec2f           planet_texture_scale(1.0f, 1.0f);  /* Scale for the planet texture */
float			spin = 0.2f;			           /* Angular velocity of the sun (radians/s) */

/* Geometries, vertexarrays and shaders relevant to the assignment */
Geometry		*shape;                     
Geometry		*sphere;
VertexArray		*shape_va;
VertexArray		*sphere_va;                        
ShaderProgram	*shape_shader;			           /* To add textures we need a shader program (vertex + fragment shader) */
ShaderProgram   *planet_shader;
Texture			*shape_texture;			           /* Sun texture */
Texture         *fieldstone_texture;
Texture			*shape_bump_map;			       /* Sun texture */
Texture         *space_ship_texture;               /* The space ship's texture */
Texture         *earth_texture;
Geometry        *space_ship;                       /* The space ship */
Geometry *reflection_sphere;
VertexArray *reflection_sphere_va;
ShaderProgram *reflection_shader;

/* Cycles */
CyclicMovement		*cycle;
CyclicMovement		*mcycle;

/* Control points */
#define	NPOINTS		(13)
vec3f		cp[NPOINTS] = { vec3f(0,0,0), vec3f(0,1,3), vec3f(0,7,3), vec3f(0,7,-3), vec3f(0,1,-3), 
				vec3f(0,0,0), vec3f(-1,1,10), vec3f(7,3,10), vec3f(15,0,0), 
				vec3f(10,-5,-7), vec3f(10,-3,-10), vec3f(0,1,-10), vec3f(-2,3,-3) };	
vec3f		mcp[NPOINTS];

/* Options */
enum VertexArrayLayout {
	TRIANGLES_LAYOUT,
	LINES_LAYOUT
};

enum CullingModes {
	BACKFACE_CULLING,
	FRONTFACE_CULLING,
	NO_CULLING	
};

int cull_mode = BACKFACE_CULLING;
int i_texture = 0;
float shader_scale = 0;

bool *previousKeyState = new bool[RC_KEY_LAST];

bool is_key_released(bool * prevkeys, bool * keys, size_t key)
{
    return prevkeys[key] == 1 && keys[key] == 0;
}

void setCulling(ShaderProgram *shader, int culling_mode) {
	switch (culling_mode) {
		case BACKFACE_CULLING:
			// cull backfacing triangles
			shader->getRenderState()->enableCulling(CULL_BACK);
			break;
		case FRONTFACE_CULLING:
			// cull front-facing triangles
			shader->getRenderState()->enableCulling(CULL_FRONT);	
			break;
		case NO_CULLING:
			// No triangle culling
			shader->getRenderState()->disableCulling();	
			break;
		default:			
			break;
	}	
}

ShaderProgram *create_torus_shader() 
{
    /* Create a shader and set its attributes */
    ShaderProgram *tmp_shader = SceneGraph::createShaderProgram("shader", "JGBumpMap", 0);
	tmp_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	tmp_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
	tmp_shader->setVector("AmbientColor", ambient.vec, 3, UNIFORM_FLOAT32);	
	tmp_shader->setVector("SpecularColor", specular.vec, 3, UNIFORM_FLOAT32);
	tmp_shader->setScalar("Shininess", 10.0f, UNIFORM_FLOAT32);
	tmp_shader->setVector("Scale", shape_texture_scale.vec, 2, UNIFORM_FLOAT32);

    // Set textures
    Texture *diffuse_tex = SceneGraph::createTexture("fieldstone", "textures/Fieldstone_diffuse.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
    Texture *bump_tex    = SceneGraph::createTexture("fieldstone_bump_map", "textures/Fieldstone_bump.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
    tmp_shader->setTexture("BumpMap", bump_tex);
    tmp_shader->setTexture("DiffuseTex", diffuse_tex);
    
	// Set culling mode
	setCulling(tmp_shader, cull_mode);

	return tmp_shader;
}

void create_torus()
{
	// Create vertex array
    shape_va = createTorus(300, 400, 5, 1);

    // create shape and add it to world
    shape = SceneGraph::createGeometry("shape", shape_va, false);	
    world->attachChild(shape);

    shape_shader = create_torus_shader();

    /* Assign shader and texture to the torus */
    shape->setShaderProgram(shape_shader);
    
}

void create_path() 
{
    // Create a cycle	
    cycle = new CyclicMovement("Cycle1", world, cp, NPOINTS);	
    cycle->setInterpolationMode(CyclicMovement::CATMULROM);
    cycle->setTension(0.6f);

    // Create the mirror cycle
    for (int i = 0; i < NPOINTS; ++i) {
        mcp[i] = vec3f(cp[i]);
        mcp[i].x = -mcp[i].x;
    }

    mcycle = new CyclicMovement("Cycle2", world, mcp, NPOINTS);	
    mcycle->setInterpolationMode(CyclicMovement::CATMULROM);
    mcycle->setTension(0.6f);
}

ShaderProgram *create_planet_shader() 
{
	// Create shader and settings
    ShaderProgram *planet_shader = SceneGraph::createShaderProgram("planet_shader", "JGBumpMap", 0);
    planet_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
    planet_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
	planet_shader->setVector("AmbientColor", ambient.vec, 3, UNIFORM_FLOAT32);	
    planet_shader->setVector("DiffuseColor", ambient.vec, 3, UNIFORM_FLOAT32);	
    planet_shader->setVector("SpecularColor", specular.vec, 3, UNIFORM_FLOAT32);
    planet_shader->setScalar("Shininess", 10.0f, UNIFORM_FLOAT32);
    planet_shader->setVector("Scale", planet_texture_scale.vec, 2, UNIFORM_FLOAT32);
	
	earth_texture = SceneGraph::createTexture("earth_texture", "textures/Earth.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	Texture *earth_bump = SceneGraph::createTexture("earth_texture", "textures/earth_bump.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	sphere->setTexture("DiffuseTex", earth_texture);
	sphere->setTexture("BumpMap", earth_bump);

    setCulling(planet_shader, cull_mode);

    return planet_shader;
}

void create_ship_and_planet() 
{
    // create vertex array
    sphere_va = createSphere(60, 30, 1);

	// Create geometry
    sphere = SceneGraph::createGeometry("sphere", sphere_va, false);
	
	// Create and set shader
    planet_shader = create_planet_shader();
    sphere->setShaderProgram(planet_shader);
    cycle->getCycleGroup()->attachChild(sphere);

    // Create a planet and add to mcycle
    space_ship = SceneGraph::createGeometry("spaceship", "scenes/spaceship.obj");    
    space_ship_texture = SceneGraph::createTexture("spaceship_texture", "textures/metal003.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
    space_ship->setTexture("DiffuseTex",space_ship_texture);
    space_ship->setShaderProgram(planet_shader);
    space_ship->rotateZ(fPI/2);
    mcycle->attachChild(space_ship);
}

ShaderProgram *create_cubemap_shader() 
{
    ShaderProgram *reflection_shader = SceneGraph::createShaderProgram("cube map shader", "JGCubeBumpMap", 0);
    reflection_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
    reflection_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);

    return reflection_shader;
}
CubeMap *reflection_cube_map;

void create_cubemap_torus() 
{
    reflection_sphere_va = createTorus(100, 100, 2, 1);
    reflection_sphere = SceneGraph::createGeometry("reflection sphere", reflection_sphere_va, false);

    reflection_sphere->setShaderProgram(create_cubemap_shader());

    reflection_cube_map = SceneGraph::createCubeMap("debug reflection cube map", 
        "textures/nvlobby_new_cubemap/nvlobby_new_negx.png", 
        "textures/nvlobby_new_cubemap/nvlobby_new_posx.png", 
        "textures/nvlobby_new_cubemap/nvlobby_new_posy.png", 
        "textures/nvlobby_new_cubemap/nvlobby_new_negy.png", 
        "textures/nvlobby_new_cubemap/nvlobby_new_posz.png", 
        "textures/nvlobby_new_cubemap/nvlobby_new_negz.png");
        //"textures/debug_cubemap/debug_posx.png", 
        //"textures/debug_cubemap/debug_negx.png", 
        //"textures/debug_cubemap/debug_negy.png", 
        //"textures/debug_cubemap/debug_posy.png", 
        //"textures/debug_cubemap/debug_negz.png", 
        //"textures/debug_cubemap/debug_posz.png");

    reflection_sphere->setCubeMap("debug reflection cube map", reflection_cube_map);
    Texture *earth_bump = SceneGraph::createTexture("earth bump map", "textures/earth_bump.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
    reflection_sphere->setTexture("BumpMap", earth_bump);
    
    world->attachChild(reflection_sphere);
}

void RCInit()
{
	world = SceneGraph::createWorld("solar_system");

	/* Camera */
	camera = SceneGraph::createCamera("cam1");
	camera->setPerspectiveProjection(1.2f, 4.0f / 3.0f, 1.0f, 1000.0f);
	camera->translate(0.0f, 1.0f, 6.0f);
	world->setActiveCamera(camera);

	/* Create pivot */
	camera_pivot = SceneGraph::createGroup("camera_pivot");
	camera_pivot->attachChild(camera);
	world->attachChild(camera_pivot);

	/* light */
	light = SceneGraph::createLight("light1");
	light->setColor(1.0f, 1.0f, 1.0f);
	light->setTranslate(15.0f, 15.0f, 15.0f);
	world->attachChild(light);	

	/* Create vertex array for circle ring */

    create_torus();

	/* Set clear color and depth */
	Renderer::setClearColor(vec4f(0.1f, 0.1f, 0.1f, 1.0f));
	Renderer::setClearDepth(1.0f);
    
	/* Enable XZ-grid and world axes */
	Command::run("/grid 1");
	Command::run("/axis_origin 1");
	
    create_path();
		
    create_ship_and_planet();

    create_cubemap_torus();

}


void attatch_camera_to(Node *n) 
{
	if (camera_pivot->getParent() != n) {
		camera_pivot->setTranslate(0.0f, 0.0f, 0.0f);
		camera_pivot->detachFromParent();		
		n->attachChild(camera_pivot);
	}	
}

u32 RCUpdate()
{
	
	cycle->update(Platform::getFrameTimeStep());
	mcycle->update(Platform::getFrameTimeStep());

    vec3f direction = space_ship->getWorldPosition();
    direction.normalize();
    
    //if (direction.length() > 0)
    //    spaceShip->rotate(fPI/50, direction);
	//shape->rotateZ( spin * Platform::getFrameTimeStep() );


	/* Mouse & keyboard controls --> */

	/* Get the current mouse button state */
	bool *mouse = Platform::getMouseButtonState();

	/* Get mouse screen coordinates [-1, 1] */
	vec2f mouse_pos = Platform::getMousePosition();

	/* Translate mouse position and button state to camera rotations */
	if (mouse[RC_MOUSE_LEFT]) {

		/* Absolute position */

		camera_rotation.x = -mouse_pos.x * 2.0f;
		camera_rotation.y = mouse_pos.y * 2.0f;

	} else if (mouse[RC_MOUSE_RIGHT]) {

		/* Relative position */

		vec2f mouse_diff = mouse_pos - mouse_prev_pos;
		camera_rotation.x -= mouse_diff.x * 2.0f;
		camera_rotation.y += mouse_diff.y * 2.0f;
	}

	/* Perform camera rotations */
	camera_pivot->setRotateX(camera_rotation.y);
	camera_pivot->rotateY(camera_rotation.x);

	/* Store previous mouse screen position */
	mouse_prev_pos = mouse_pos;



	/* Get the current key state */
	bool *keys = Platform::getKeyState();

	/* Map keys to bilateral camera movement */
	f32 move = 0.0f, strafe = 0.0f;
	if (keys[RC_KEY_W])	move += 0.1f;
	if (keys[RC_KEY_S])	move -= 0.1f;
	if (keys[RC_KEY_A])	strafe -= 0.1f;
	if (keys[RC_KEY_D])	strafe += 0.1f;
	if (keys[RC_KEY_ESCAPE]) 
    {
        RCDestroy();
        exit(0);
    }

	/* Apply movement to camera */
	camera->translate(camera->getLocalFront() * move);
	camera->translate(camera->getLocalRight() * strafe);

	/* Attatch camera pivot to world or objects */
	if (keys[RC_KEY_0]) attatch_camera_to(world);		
	if (keys[RC_KEY_1]) attatch_camera_to(space_ship);
    if (keys[RC_KEY_2]) attatch_camera_to(sphere);
    

	/* Clear color and depth buffers */
	Renderer::clearColor();
	Renderer::clearDepth();

	/* Tell RenderChimp to render the scenegraph */
	world->renderAll();

	memcpy(previousKeyState, keys, 180);

	return 0;
}

void RCDestroy()
{
    delete previousKeyState;
	delete cycle;
	delete mcycle;
	
	// release memory allocated by the scenegraph
	SceneGraph::deleteNode(world);
}

#endif /* ASSIGNMENT_2 */

