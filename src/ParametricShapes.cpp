

#include "RenderChimp.h"
#include <math.h>
#include <stdio.h>


//#define DEBUG_PR



/*
 Circle ring
 
 @param res_radius
 tessellation resolution (nbr of vertices) in the radial direction ( inner_radius < radius < outer_radius )
 
 @param res_theta
 tessellation resolution (nbr of vertices) in the angular direction ( 0 < theta < 2PI )
 
 @param inner_radius
 radius of the innermost border of the ring
 
 @param outer_radius
 radius of the outermost border of the ring
 
 @return
 pointer to the generated VertexArray object 
 
 CJG aug 2010 - created
 */
VertexArray* createCircleRing(const i32 &res_radius, const i32 &res_theta, const f32 &inner_radius, const f32 &outer_radius)
{
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,			/* spatial coordinates */
		texx, texy, texz,	/* texture coordinates */
		nx, ny, nz,		/* normal vector */
		tx, ty, tz,		/* tangent vector */
		bx, by, bz;		/* binormal vector */
	};
	
	/* vertex array */
	Vertex	*va = new Vertex[res_radius*res_theta];
	
	f32 theta = 0.0f,			/* 'stepping'-variable for theta: will go 0 - 2PI */
	dtheta = 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */
	
	f32	radius,						/* 'stepping'-variable for radius: will go inner_radius - outer_radius */
	dradius = (outer_radius-inner_radius)/(res_radius-1);	/* step size, depending on the resolution */
	
	/* generate vertices iteratively */
	for (int i = 0; i < res_theta; i++)
	{
		f32	cos_theta = cosf(theta),
		sin_theta = sinf(theta);
		radius = inner_radius;
		
		for (int j = 0; j < res_radius; j++)
		{
			i32 vertex_index = res_radius*i + j;
			
			/* vertex */
			va[vertex_index].x = radius * cos_theta;
			va[vertex_index].y = radius * sin_theta;
			va[vertex_index].z = 0.0f;
			
			/* texture coordinates */
			va[vertex_index].texx = (f32)j/(res_radius-1);
			va[vertex_index].texy = (f32)i/(res_theta-1);
			va[vertex_index].texz = 0.0f;
			
			/* tangent */
			vec3f t = vec3f(cos_theta,
					sin_theta,
					0);
//			t.normalize(); // already normalized
			
			/* binormal */
			vec3f b = vec3f(-sin_theta,
					cos_theta,
					0);			
//			b.normalize(); // already normalized
			
			/* normal */
			vec3f n = t.cross( b );
			
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;
			
			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;
			
			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;
			
			radius += dradius;
		}
		
		theta += dtheta;
	}
	
	/* triangle def */
	struct Triangle
	{
		i32 a, b, c;		/* vertex indices */
	};
	
	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_radius-1) ];
	
	/* generate indices iteratively */
	for (int i = 0; i < res_theta-1; i++ )
	{
		for (int j = 0; j < res_radius-1; j++ )
		{
			
			int tri_index = 2 * ((res_radius-1)*i + j);
			
			ia[tri_index].a = res_radius*i+j;
			ia[tri_index].b = res_radius*i+j + 1;
			ia[tri_index].c = res_radius*i+j + 1+res_radius;
			
			ia[tri_index+1].a = res_radius*i+j;
			ia[tri_index+1].b = res_radius*i+j + 1+res_radius;
			ia[tri_index+1].c = res_radius*i+j + res_radius;	
		}
	}
	
	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *cring_va = SceneGraph::createVertexArray(0, va, sizeof(Vertex), res_radius*res_theta, TRIANGLES, USAGE_STATIC);
	cring_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);
	
	/* set the index array */
	cring_va->setIndexArray(ia, sizeof(i32), 2*3*(res_radius-1)*(res_theta-1) );
	
	delete[] va; // Fixed memory leak...
	delete[] ia;
	
	return cring_va;
}


/*
 Sphere
 
 Author(s): Jakob Grundström, Bengt Ericsson
 */
VertexArray* createSphere(const i32 &res_theta, const i32 &res_phi, const f32 &radius)
{
	f32	theta, dtheta, phi, dphi, cos_theta, sin_theta, cos_phi, sin_phi;
	i32	x, y, vertex_index, tri_index;
	
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,		/* spatial coordinates */
		texx, texy, texz,	/* texture coordinates */
		nx, ny, nz,		/* normal vector */
		tx, ty, tz,		/* tangent vector */
		bx, by, bz;		/* binormal vector */
	};
	
	/* vertex array */
	Vertex *va = new Vertex[res_theta*res_phi];
	
	theta = 0;				/* 'stepping'-variable for theta: will go 0 - 2PI */
	dtheta = 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */
	
	phi = 0;				/* 'stepping'-variable for phi: will go 0 - PI */
	dphi = fPI/(res_phi-1);			/* step size, depending on the resolution */
	
	
	/* pre-compute cos and sines */
	f32 *sin_thetas = new f32[res_theta];
	f32 *cos_thetas = new f32[res_theta];
	for (int x = 0; x < res_theta; ++x) {		
		cos_thetas[x] = cosf(theta);
		sin_thetas[x] = sinf(theta);
		theta += dtheta;
	}
	
	/* generate vertices iteratively */	
	
	phi	= 0;
	
	for (y = 0; y < res_phi; ++y)
	{		
		cos_phi   = cosf(phi);	
		sin_phi   = sinf(phi);	
		
		for (x = 0; x < res_theta; ++x)
		{	
			vertex_index = res_theta*y + x;
			
			cos_theta = cos_thetas[x];
			sin_theta = sin_thetas[x];
			
			/* vertex */
			va[vertex_index].x =   radius * sin_theta * sin_phi;
			va[vertex_index].y = - radius * cos_phi;
			va[vertex_index].z =   radius * cos_theta * sin_phi;
			
#ifdef DEBUG_PR			
			printf("[%d, %d] = [%d] = (%4.1f, %4.1f, %4.1f)\n", x, y, vertex_index, va[vertex_index].x,va[vertex_index].y,va[vertex_index].z);
#endif
			
			// texture coordinates
			va[vertex_index].texx = (f32)x/(res_theta-1);
			va[vertex_index].texy = (f32)y/(res_phi-1);
			va[vertex_index].texz = 0.0f;
			
			// tangent
            vec3f t = vec3f(cos_theta,
                0,
                -sin_theta);

			t.normalize();
			
			// binormal
			vec3f b = vec3f(sin_theta*cos_phi,
					sin_phi,
					cos_theta*cos_phi);
			b.normalize();
	
			/* normal */
			vec3f n = t.cross( b );
            
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;
			
			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;
			
			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;
			
		}
#ifdef DEBUG_PR		
		puts("");
#endif				
		phi += dphi;
		
	}	
	
	/* triangle def */
	struct Triangle 
	{
		i32 a, b, c;		/* vertex indices */
	};
	
	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_phi-1) ];
	
#ifdef DEBUG_PR		 
	printf("ntriangles = %d\n", 2*(res_theta-1)*(res_phi-1));
#endif
	
	/* generate indices iteratively */
	
	for (y = 0; y < res_phi-1; ++y)
	{				
		i32 row = res_theta*y;
		
		for (x = 0; x < res_theta-1; ++x)
		{			
			tri_index = 2 * ( (res_theta-1)*y + x );
			
			i32 next_row = row + res_theta;			
			i32 a,b,c,d;
			
			a = row		+ x;
			b = row		+ (x+1);
			c = next_row	+ (x+1);
			d = next_row	+ x;	
			
			ia[tri_index].a = a;
			ia[tri_index].b = c;
			ia[tri_index].c = d;
			
			ia[tri_index+1].a = a;
			ia[tri_index+1].b = b;
			ia[tri_index+1].c = c;			
#ifdef DEBUG_PR			
			printf("tri_index   = %d\n", tri_index);
			printf("tri_index+1 = %d\n", tri_index+1);
			printf("[a,b,c,d] = [%d %d %d %d]\n", a,b,c,d);
#endif
		}
	}
	
	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */		
	//VertexArray *sphere_va = SceneGraph::createVertexArray(0, va, sizeof(Vertex), res_theta*res_phi, LINE_STRIP, USAGE_STATIC);
	VertexArray *sphere_va = SceneGraph::createVertexArray(0, va, sizeof(Vertex), res_theta*res_phi, TRIANGLES, USAGE_STATIC);

	sphere_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);
	
	/* set the index array */
	sphere_va->setIndexArray(ia, sizeof(i32), 3*2*(res_theta-1)*(res_phi-1));
	
	delete[] va;
	delete[] ia;
    delete[] cos_thetas;
	delete[] sin_thetas;

	return sphere_va;
}




/*
 Torus
 
 Author(s): Jakob Grundström, Bengt Ericsson
 */
VertexArray* createTorus(const i32 &res_theta, const i32 &res_phi, const f32 &rA, const f32 &rB)
{
	f32	theta, dtheta, phi, dphi, cos_theta, sin_theta, cos_phi, sin_phi;
	i32	x, y, vertex_index, tri_index;
	
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,		/* spatial coordinates */
		texx, texy, texz,	/* texture coordinates */
		nx, ny, nz,		/* normal vector */
		tx, ty, tz,		/* tangent vector */
		bx, by, bz;		/* binormal vector */
	};
	
	/* vertex array */
	Vertex *va = new Vertex[res_theta*res_phi];
	
	theta = phi = 0;			/* 'stepping'-variable for theta: will go 0 - 2PI */
	dtheta	= 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */
	dphi	= 2.0f*fPI/(res_phi-1); 
		
	/* pre-compute cos and sines */
    f32 *sin_thetas = new f32[res_theta];
    f32 *cos_thetas = new f32[res_theta];
	for (int x = 0; x < res_theta; ++x) {		
		cos_thetas[x] = cosf(theta);
		sin_thetas[x] = sinf(theta);
		theta += dtheta;
	}
	
	/* generate vertices iteratively */	
	
	phi	= 0;
	
	for (y = 0; y < res_phi; ++y)
	{		
		cos_phi   = cosf(phi);	
		sin_phi   = sinf(phi);	
		
		for (x = 0; x < res_theta; ++x)
		{	
			vertex_index = res_theta*y + x;
			
			cos_theta = cos_thetas[x];
			sin_theta = sin_thetas[x];
			
			/* vertex */
			va[vertex_index].x =   (rA + rB*cos_theta) * cos_phi;
			va[vertex_index].y =   (rA + rB*cos_theta) * sin_phi; 			
			va[vertex_index].z = -	rB * sin_theta;
			
#ifdef DEBUG_PR			
			printf("[%d, %d] = [%d] = (%4.1f, %4.1f, %4.1f)\n", x, y, vertex_index, va[vertex_index].x,va[vertex_index].y,va[vertex_index].z);
#endif
			
			// texture coordinates
			va[vertex_index].texx = (f32)x/(res_phi-1);
			va[vertex_index].texy = (f32)y/(res_theta-1);
			va[vertex_index].texz = 0.0f;
			
			// tangent
			vec3f t = vec3f( - rB * sin_theta * cos_phi,
					 - rB * sin_theta * sin_phi,
					 - rB * cos_theta );
			t.normalize();
			
			// binormal
			vec3f b = vec3f( - (rA + rB*cos_theta) * sin_phi,
					   (rA + rB*cos_theta) * cos_phi, 
					    0 );
				
			b.normalize();
			
			/* normal */
			vec3f n = t.cross( b );
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;
			
			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;
			
			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;
						
		}
#ifdef DEBUG_PR		
		puts("");
#endif				
		phi += dphi;
		
	}	
	
	
	/* triangle def */
	struct Triangle 
	{
		i32 a, b, c;		/* vertex indices */
	};
	
	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_phi-1) ];
	
#ifdef DEBUG_PR		 
	printf("ntriangles = %d\n", 2*(res_theta-1)*(res_phi-1));
#endif
	
	/* generate indices iteratively */
	
	for (y = 0; y < res_phi-1; ++y)
	{		
		i32 row = res_theta * y;
		
		for (x = 0; x < res_theta-1; ++x)
		{			
			tri_index = 2 * ( (res_theta-1)*y + x );
		
			i32 next_row = row + res_theta;
			i32 a,b,c,d;
			
			a = row		+ x;
			b = row		+ (x+1);
			c = next_row	+ (x+1);
			d = next_row	+ x;	
						
			ia[tri_index].a = a;
			ia[tri_index].b = c;
			ia[tri_index].c = d;
			
			ia[tri_index+1].a = a;
			ia[tri_index+1].b = b;
			ia[tri_index+1].c = c;			
#ifdef DEBUG_PR			
			printf("tri_index   = %d\n", tri_index);
			printf("tri_index+1 = %d\n", tri_index+1);
			printf("[a,b,c,d] = [%d %d %d %d]\n", a,b,c,d);
#endif				
		}
	}
	
	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	//VertexArray *torus_va = SceneGraph::createVertexArray(0, va, sizeof(Vertex), res_theta*res_phi, LINE_STRIP, USAGE_STATIC);
	VertexArray *torus_va = SceneGraph::createVertexArray(0, va, sizeof(Vertex), res_theta*res_phi, TRIANGLES, USAGE_STATIC);

	torus_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);
	
	/* set the index array */
	torus_va->setIndexArray(ia, sizeof(i32), 3*2*(res_theta-1)*(res_phi-1));
	
	delete[] va;
	delete[] ia;
    delete[] cos_thetas;
    delete[] sin_thetas;

	return torus_va;	
}


