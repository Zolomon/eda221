
#include "program.h"

#if defined ASSIGNMENT_1

/*
	EDA221 Introduction to Computer Graphics, 2011

	Assignment 1 - Solar System
*/

#include "RenderChimp.h"
#include "Command.h"
#include <map>
#include <string>
#include <iostream>
#include "Planet.h"
#include <vector> 
#include "ParametricShapes.h"

using namespace std;
using namespace assign1;

World			*world;				/* Scene graph root node */
Camera			*camera;			/* Scene camera */
Group			*camera_pivot;		/* A group is an empty, transformable node */
Light			*light;				/* A light is a point in the scene which can be used for lighting calculations */

Geometry		*sun;		/* Sun node */

vec3f			 sun_ambient(1.0f, 1.0f, 1.0f);	/* Ambient color for the sun (never gets dark!) */

ShaderProgram	*sun_shader;		/* To add textures we need a shader program (vertex + fragment shader) */
Texture			*sun_texture;		/* Sun texture */

float			 sun_spin = 0.4f;	/* Angular velocity (spin) of the sun (radians/s) */

string planetNames[] = {"Sun", "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"};

f32 scales[] = { 1.0f, 0.25f, 0.62f, 0.65f, 0.18f, 1.35f, 1.30f, 1.10f, 1.60f, 1.50f };
f32 positions[] = {0.0f, 1.5f, 3.0f, 5.0f, 7.0f, 9.0f, 11.0f, 13.0f, 15.0f};
f32 planet_tilt_angles[] = {0.0f, 0.0f, 0.0f, fPI/6, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
f32 planet_orbit_angles[] = {0.0f, fPI/8, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

bool *previousKeyState = new bool[RC_KEY_LAST];
bool fps = false;

enum PlanetType {
	Sun = 0,
	Mercury,
	Venus,
	Earth,
	Mars,
	Jupiter,
	Saturn,
	Uranus,
	Neptune
	};

vector<Planet *> planets;

/* variables used for mouse-camera control */
vec2f	camera_rotation(0.0f, 0.0f),	/* current rotation of camera */
		mouse_prev_pos(0.0f, 0.0f);		/* previous mouse position */

Planet *CreatePlanet(PlanetType id, string name, Node *world) 
{
	Planet *planet = new Planet(name, world);
	return planet;
}

void InitPlanets(Node *world, Light *light)
{
	vec3f planet_ambient = vec3f(0.05f, 0.05f, 0.05f);
	
	ShaderProgram *shader = SceneGraph::createShaderProgram("planet_shader", "LambertTexture", 0);
	shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
	shader->setVector("Ambient", planet_ambient.vec, 3, UNIFORM_FLOAT32);

    ShaderProgram *transparent_shader = SceneGraph::createShaderProgram("planet_shader_transparent", "LambertTextureTransparent", 0);
    transparent_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
    transparent_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
    transparent_shader->setVector("Ambient", planet_ambient.vec, 3, UNIFORM_FLOAT32);
    
	for (PlanetType planet = Mercury; planet < Neptune; planet = static_cast<PlanetType>(planet+1)) {
		Planet *p = CreatePlanet(planet, planetNames[planet], world);

		p->setShader(shader);
		string textureName = "textures/"+ planetNames[planet] +".jpg";
		p->setTexture(textureName.c_str());
		p->translate(positions[planet]*2.5f,0,0);
		p->scale(scales[planet],scales[planet],scales[planet]);
		
        p->setAxisTilt(planet_tilt_angles[planet]);
        p->setAxisRotation(fPI/10*(9-planet));

        p->setOrbitTilt(planet_orbit_angles[planet]);
		p->setOrbitRotation((fPI/2)/20*(9-planet));

        if (planet == Saturn)
            p->setRings("textures/saturn_rings.jpg", transparent_shader, fPI/80, 50, 50, 1.3f, 2.5f);
		if (planet == Uranus)
            p->setRings("textures/uranus_rings.jpg", transparent_shader, fPI/60, 50, 50, 1.2f, 2.6f);
        if (planet == Earth)
            p->addMoon("textures/metal003.jpg", shader, 0.3f, 1.5f, fPI/20, fPI/20, fPI/60, fPI/50);
        if (planet == Jupiter)
        {
            p->addMoon("textures/metal003.jpg", shader, 0.10f, 3.5f, fPI/20, fPI/20, fPI/29, fPI/100);
            p->addMoon("textures/metal003.jpg", shader, 0.09f, 3.5f, fPI/20, fPI/20, fPI/10, fPI/400);
            p->addMoon("textures/metal003.jpg", shader, 0.12f, 2.5f, fPI/20, fPI/20, fPI/14, fPI/300);
            p->addMoon("textures/metal003.jpg", shader, 0.08f, 3.1f, fPI/20, fPI/20, fPI/9, fPI/350);
            p->addMoon("textures/metal003.jpg", shader, 0.15f, 1.6f, fPI/20, fPI/20, fPI/36, fPI/200);
            p->addMoon("textures/metal003.jpg", shader, 0.04f, 1.7f, fPI/20, fPI/20, fPI/34, fPI/190);
            p->addMoon("textures/metal003.jpg", shader, 0.07f, 2.5f, fPI/20, fPI/20, fPI/7, fPI/800);
        }

		planets.push_back(p);
	}
	
}

Geometry *space_ship;

void RCInit()
{
	/* Create world node */
	world = SceneGraph::createWorld("solar_system");

	/* TODO: Create camera */
	camera = SceneGraph::createCamera("MyCamera");
	/* Set camera perspective projection (field-of-view, aspect ratio, near-plane. far-plane) */
	camera->setPerspectiveProjection(1.2f, 4.0f / 3.0f, 1.0f, 10000.0f);
	/* TODO: Translate the camera to (0, 1, 6) */
	camera->translate(0.0f, 0.0f, 40.0f);
	/* Make this camera the active camera */
	world->setActiveCamera(camera);

	/* Create camera pivot group */
	camera_pivot = SceneGraph::createGroup("camera_pivot");
	/* Attach camera */
	camera_pivot->attachChild(camera);
	/* Attach to world */
	world->attachChild(camera_pivot);

	/* TODO: Create light source */
	light = SceneGraph::createLight("MyLight");
	/* TODO: Set the light color to (1, 1, 1) */
	light->setColor(1.0f, 1.0f, 1.0f);
	/* TODO: Translate the light to (10, 10, 10) */
	light->translate(10.0f, 10.0f, 10.0f);
	/* TODO: Attach the light to the world */
	
	world->attachChild(light);

	/* Create sun geometry from a sphere object-file and attach it to world */
	sun = SceneGraph::createGeometry("sun", "scenes/sphere.obj");
	sun->scale(scales[Sun],scales[Sun],scales[Sun]);
	world->attachChild(sun);

	/* TODO: Build & run (F5 in Visual Studio) */

	/* Create a shader, set its attributes and attach to sun (uncomment when reaching this point) */
	sun_shader = SceneGraph::createShaderProgram("sun_shader", "LambertTexture", 0);
	sun_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	sun_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
	sun_shader->setVector("Ambient", sun_ambient.vec, 3, UNIFORM_FLOAT32);
	sun->setShaderProgram(sun_shader);
	
	/* Create a texture from file and assign to the sun (uncomment when reaching this point) */
	sun_texture = SceneGraph::createTexture("sun_texture", "textures/sunmap.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	sun->setTexture("DiffuseTex", sun_texture);

	Group *ship_pivot = SceneGraph::createGroup("ship_pivot");
	

	Geometry *x_arrow = SceneGraph::createGeometry("x_arrow", "scenes/arrow.obj");
	Geometry *y_arrow = SceneGraph::createGeometry("x_arrow", "scenes/arrow.obj");
	Geometry *z_arrow = SceneGraph::createGeometry("x_arrow", "scenes/arrow.obj");
	x_arrow->setShaderProgram(sun_shader);
	y_arrow->setShaderProgram(sun_shader);
	z_arrow->setShaderProgram(sun_shader);
	Texture *x_texture = SceneGraph::createTexture("x_texture", "textures/tiles.png", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	Texture *y_texture = SceneGraph::createTexture("y_texture", "textures/venus.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	Texture *z_texture = SceneGraph::createTexture("z_texture", "textures/waves.png", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	x_arrow->setTexture("DiffuseTex", x_texture);
	y_arrow->setTexture("DiffuseTex", y_texture);
	z_arrow->setTexture("DiffuseTex", z_texture);
	ship_pivot->attachChild(x_arrow);
	ship_pivot->attachChild(y_arrow);
	ship_pivot->attachChild(z_arrow);
	x_arrow->rotateZ(-fPI/2.0f);
	z_arrow->rotateX(fPI/2.0f);
	world->attachChild(ship_pivot);
	ship_pivot->translate(-3,3,-3);


	/* TODO: Build & run (F5 in Visual Studio) */
	space_ship = SceneGraph::createGeometry("Test", "scenes/spaceship.obj");
	/*spaceShip->translate(0, 5, 0);
	spaceShip->rotate(45, 0,0,0);
	spaceShip->setScale(10);*/
	space_ship->setShaderProgram(sun_shader);
	Texture *ship_texture = SceneGraph::createTexture("ship_texture", "textures/tiles.png", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	space_ship->setTexture("DiffuseTex", ship_texture);
	space_ship->rotateY(fPI/2);
	space_ship->translate(2,0,2);
	space_ship->setScale(0.5f);
	ship_pivot->attachChild(space_ship);

	/* TODO: Create rest of the solar system... */
	InitPlanets(world, light);

	/* Set clear color and depth */
	Renderer::setClearColor(vec4f(0.1f, 0.1f, 0.1f, 1.0f));
	Renderer::setClearDepth(1.0f);

	/* Enable XZ-grid and world axes */
	Command::run("/grid 1");
	Command::run("/axis_origin 1");
}

bool is_key_released(bool * prevkeys, bool * keys, size_t key)
{
    return prevkeys[key] == 1 && keys[key] == 0;
}

void attach_camera_to_planet( PlanetType planet ) 
{
    camera_pivot->detachFromParent();
    planets[planet-1]->attachChild(camera_pivot);
}

void camera_control( bool * prevkeys, bool * keys) 
{
    if (is_key_released(prevkeys, keys, RC_KEY_1)) attach_camera_to_planet(Mercury);
    if (is_key_released(prevkeys, keys, RC_KEY_2)) attach_camera_to_planet(Venus);
    if (is_key_released(prevkeys, keys, RC_KEY_3)) attach_camera_to_planet(Earth);
    if (is_key_released(prevkeys, keys, RC_KEY_4)) attach_camera_to_planet(Mars);
    if (is_key_released(prevkeys, keys, RC_KEY_5)) attach_camera_to_planet(Jupiter);
    if (is_key_released(prevkeys, keys, RC_KEY_6)) attach_camera_to_planet(Saturn);
    if (is_key_released(prevkeys, keys, RC_KEY_7)) attach_camera_to_planet(Uranus);
    if (is_key_released(prevkeys, keys, RC_KEY_8)) 
        attach_camera_to_planet(Neptune);
    
    if (is_key_released(prevkeys, keys, RC_KEY_0)) 
    {
        camera_pivot->detachFromParent();
        world->attachChild(camera_pivot);
    }

    if (!fps && is_key_released(prevkeys, keys, RC_KEY_C)) 
        fps = true;
    else if (fps && is_key_released(prevkeys, keys, RC_KEY_C)) 
        fps = false;
}

u32 RCUpdate()
{
	/* Apply rotation to the sun */
	sun->rotateY( sun_spin * Platform::getFrameTimeStep() );

	for(vector<Planet *>::iterator it = planets.begin(); it < planets.end(); it++) {
		(*it)->update(Platform::getFrameTimeStep());
	}
	
	/* TODO: Perform animations for the rest of the solar system */
	
	/* Mouse & keyboard controls --> */

	/* Get the current mouse button state */
	bool *mouse = Platform::getMouseButtonState();

	/* Get mouse screen coordinates [-1, 1] */
	vec2f mouse_pos = Platform::getMousePosition();

	/* Translate mouse position and button state to camera rotations */
	if (mouse[RC_MOUSE_LEFT]) {

		/* Absolute position */

		camera_rotation.x = -mouse_pos.x * 2.0f;
		camera_rotation.y = mouse_pos.y * 2.0f;

	} else if (mouse[RC_MOUSE_RIGHT]) {

		/* Relative position */

		vec2f mouse_diff = mouse_pos - mouse_prev_pos;
		camera_rotation.x -= mouse_diff.x * 2.0f;
		camera_rotation.y += mouse_diff.y * 2.0f;
	}

	/* Perform camera rotations (pivot style) */
    if (!fps)
    {
        camera_pivot->setRotateX(camera_rotation.y);
        camera_pivot->rotateY(camera_rotation.x);
    } else 
    {
        camera->setRotateX(camera_rotation.y);
        camera->rotateY(camera_rotation.x);
    }

	/* Store previous mouse screen position */
	mouse_prev_pos = mouse_pos;


	/* Get the current key state */
	bool *keys = Platform::getKeyState();

	/* Map keys to bilateral camera movement */
	f32 move = 0.0f, strafe = 0.0f;
	if (keys[RC_KEY_W])	move += 0.1f;
	if (keys[RC_KEY_S])	move -= 0.1f;
	if (keys[RC_KEY_A])	strafe -= 0.1f;
	if (keys[RC_KEY_D])	strafe += 0.1f;
	if (keys[RC_KEY_ESCAPE]) exit(0);

    camera_control(previousKeyState, keys);


	/* Apply movement to camera */
	camera->translate(camera->getLocalFront() * move);
	camera->translate(camera->getLocalRight() * strafe);

	/* Clear color and depth buffers */
	Renderer::clearColor();
	Renderer::clearDepth();

	/* tell RenderChimp to render the scenegraph */
	world->renderAll();

    memcpy(previousKeyState, keys, 180);
	return 0;
}

void RCDestroy()
{
	/* release memory allocated by the scenegraph */
	SceneGraph::deleteNode(world);
}

#endif /* ASSIGNMENT_1 */
