/*
 *  CyclicMovement.h
 *  RenderChimpOSX
 *
 *  Created by Jakob Grundström on 2012-09-22.
 *  Copyright 2012 jakobgrundstrom. All rights reserved.
 *
 */

#ifndef cyclicmovement_h
#define	cyclicmovement_h

#include "VectorMath.h"
#include "Interpolation.h"
#include <vector>

using std::vector;

namespace assign2 {

class CyclicMovement {
public:
	CyclicMovement(const char * name, Node *parent, vec3f *cp, i32 n);
	~CyclicMovement();

	void setControlPoints(vec3f *cp, i32 n);
	
	enum InterpolationType {
		LERP,
		CATMULROM
	};
		
	void setInterpolationMode(InterpolationType mode);
	void setVelocity(f32 velocity);
	void setTension(f32 tension);

	void attachChild(Transformable *t);
	void detachChild(Transformable *t);
	Group *getCycleGroup() { return cycle_group; };
	
	void update(f32 time);
private:
	inline i32 getIncrement(i32 index) 
	{
		if (++index >= npoints) {
			index = 0;
		}
		return index;
	}
		
	inline i32 getDecrement(i32 index) {
		return (npoints + index - 1) % npoints;
	}
	
	
	InterpolationType	 mode;
	Group			    *cycle_group;
	vec3f			    *control_points;
	f32			        *dist;
	i32			         npoints, current_point, next_point, prev_point, nextnext_point;
	f32			         velocity, tension, xval, lerpXval;
    vector<Transformable *> cycle_objects;
};

}

#endif