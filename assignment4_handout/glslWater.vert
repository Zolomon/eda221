
#version 150

/* Per-vertex input from application */
in vec3 vPosition;
in vec2 vTexCoord;

/* Uniform input from application */
/* Same value for all vertices */
uniform float time;
uniform mat4 ModelViewProjectionMatrix;
uniform vec3 vCameraPos;

/* Tangent basis */
out vec3	Binormal,
			Tangent,
			Normal;
				
/* View vector */
out vec3	View;

/* Multiple bump coordinates for animated bump mapping */
out vec2	bumpCoord0,
			bumpCoord1,
			bumpCoord2;

float G(vec3 position, float time, float amplitude, vec2 direction, float frequency, float phase, float k)
{
	return amplitude * pow(sin((direction.x * position.x + direction.y * position.z) * frequency + time * phase) * 0.5 + 0.5, k);
}

float DerivativeG(vec3 position, float time, float amplitude, vec2 direction, float frequency, float phase, float k)
{
	return 0.5 * frequency * amplitude * 
	pow(sin((direction.x * position.x + direction.y * position.z) * frequency + time * phase) * 0.5 + 0.5, k - 1.0) * 
	cos((direction.x * direction.y * position.z) * frequency + time * phase);
}

vec3 CreateWave(vec3 position, float time, float amplitude, vec2 direction, float frequency, float phase, float k)
{
	position.y =  G(position.xyz, time, amplitude, direction, frequency, phase, k);
	float dgd = DerivativeG(position.xyz, time, amplitude, direction, frequency, phase, k);
	float dgdx = dgd * direction.x;
	float dgdz = dgd * direction.y;
	
	return vec3(dgdx, position.y, dgdz);
}

void main()
{
	vec4 P = vec4(vPosition, 1.0);
	
	float A = 1.0; // Amplitude
	vec2  D = vec2(-1.0, 0.0); // Direction
	float f = 0.2; // Frequency
	float p = 0.5; // phase
	float k = 2.0; // sharpness

	vec3 B = vec3(0,0,0); 
    vec3 T = vec3(0,0,0); 
	vec3 N = vec3(0,0,0);

	vec3 sumH = vec3(0,0,0);

	/* TODO: Add waves to P */
	vec3 result = CreateWave(P.xyz, time, A, D, f, p, k);

	sumH += result;

	A = 2.5;
	D = vec2(-0.7, 0.7);
	f = 0.4;
	p = 1.3;

	result = CreateWave(P.xyz, time, A, D, f, p, k);
	sumH += result;

	/* TODO: Compute B, T, N */
    B = vec3(1, sumH.x, 0); 
    T = vec3(0, sumH.z, 1); 
	N = vec3(-sumH.x, 1, -sumH.z);
	

	P.y = sumH.y;
	Binormal = B;
	Tangent = T;
	Normal = N;

	View = normalize(vCameraPos - vPosition);

	/* TODO: Compute bumpmap coordinates */
		// Animated bump mapping
	vec2 texScale   = vec2(3, 1.4);
	float bumpTime  = mod(time, 100.0);
	vec2 bumpSpeed = vec2(-0.05, 0);

	bumpCoord0 = vTexCoord * texScale + bumpTime * bumpSpeed;
	bumpCoord1 = vTexCoord * texScale * 2 + bumpTime * bumpSpeed * 4;
	bumpCoord2 = vTexCoord * texScale * 4 + bumpTime * bumpSpeed * 8;
	
	gl_Position = ModelViewProjectionMatrix * P;
}