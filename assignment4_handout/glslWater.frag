
#version 150

out vec4 fColor;

/* Cube- and Bumpmaps */
uniform samplerCube SkyboxTexture;
uniform sampler2D   BumpMapTexture;

/* Tangent basis */
in vec3	Binormal,
		Tangent,
		Normal;
				
/* View vector */
in vec3	View;

/* Multiple bump coordinates for animated bump mapping */
in vec2	bumpCoord0,
		bumpCoord1,
		bumpCoord2;

float fastFresnel(vec3 I, vec3 N, float r0)
{
    return r0 + (1.0-r0) * pow(1.0-dot(I, N), 5.0);
}


void main()
{
	/* TODO: add color, reflection, animated bump mapping, refraction and fresnel */
    vec3 N = normalize(Normal);
	vec3 T = normalize(Tangent);
	vec3 B = normalize(Binormal);
	vec3 V = normalize(View);
	mat3 BTN = mat3(B,T,N);

	// Water color
	vec3 colorDeep = vec3(0.0, 0.0, 0.1);
	vec3 colorShallow = vec3(0.0, 0.5, 0.5);

	// Animated Bump Normal
	vec3 bumpColor0 = texture(BumpMapTexture, bumpCoord0).rgb * 2.0 - 1.0;
	vec3 bumpColor1 = texture(BumpMapTexture, bumpCoord1).rgb * 2.0 - 1.0;
	vec3 bumpColor2 = texture(BumpMapTexture, bumpCoord2).rgb * 2.0 - 1.0;

	bumpColor0 = (BTN * bumpColor0).xyz;
	bumpColor1 = (BTN * bumpColor1).xyz;
	bumpColor2 = (BTN * bumpColor2).xyz;

	vec3 bumpNormal = BTN * normalize(bumpColor0 + bumpColor1 + bumpColor2);

	float facing = 1 - max(dot(V, N), 0.0);

	vec3 colorWater = mix(colorDeep, colorShallow, facing);

	// Reflection
	vec3 R = reflect(-V, bumpNormal);
	vec3 reflection = texture(SkyboxTexture, R).rgb;

	// Fast Fresnel
	float fresnel = fastFresnel(V, colorWater, 0.02037);

	// refraction
	vec3 refraction = refract(V, colorWater, 1.33);

	fColor = vec4(	colorWater 
					+ reflection 
					* fresnel 
					+ refraction * (1-fresnel)
					, 1.0);
}